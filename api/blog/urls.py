from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from .views import *
from rest_framework_nested import routers

router = routers.DefaultRouter()
router.register('blog', BlogViewSet, basename='blog')

urlpatterns = [
	path('', include(router.urls)),
]
