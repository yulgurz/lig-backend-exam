from django.contrib.auth import authenticate
from rest_framework import status
from rest_framework.authtoken.models import Token
from user.models import User
from user.serializers import *
import jwt

JWT_SECRET = 'secret'
JWT_ALGORITHM = 'HS256'

class Users:

	def __init__(self, username, password):
		self.username = username
		self.password = password

	def authenticate_user(self):
		if self.username is None or self.password is None:
			return Response({'error': 'Please provide both email and password.'},
				status=status.HTTP_400_BAD_REQUEST)

		user = authenticate(username=self.username, password=self.password)
		if not user:
			return Response({'error': 'Invalid username or password.'},
				status=status.HTTP_404_NOT_FOUND)
		
		user = User.objects.get(id=user.id)
		self.user = UserSerializer(user, many=False).data
		return self.user

	def initialize_token(self):
		try:
			token = Token.objects.get(user_id=self.user['id'])
			self.token = token
		except:
			user_serializer = UserSerializer(self.user, many=False)

			payload = {
				'id': self.user['id'],
				'email': self.user['email'],
				'first_name': self.user['first_name'],
				'last_name': self.user['last_name']
				}

			jwt_token = jwt.encode(payload, JWT_SECRET, JWT_ALGORITHM)
			jwt_token = jwt_token.decode('utf-8')
			token = Token.objects.create(user=User.objects.get(id=self.user['id']), key=jwt_token, user_id=self.user['id'])
		
			self.token = token

		return self.token

	def get_token(self):
		return self.token.key

	def get_user(self):
		return self.user

