from django.db import models
from django.contrib.auth.models import (
	AbstractBaseUser, BaseUserManager,
)

class UserManager(BaseUserManager):
	use_in_migrations = True
	def create_user(self,email,first_name,last_name,password=None):
		if not email:
			raise ValueError("Users must have an email address")
		user = self.model(
				email=self.normalize_email(email),
				first_name=first_name,
				last_name=last_name,
			)
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_superuser(self,email,first_name,last_name,password=None):
		user = self.create_user(
				email=self.normalize_email(email),
				first_name=first_name,
				last_name=last_name,
				password=password,
			)
		user.is_admin = True
		user.is_staff = True
		user.is_superuser = True
		user.save(using=self._db)
		return user


class User(AbstractBaseUser):
	email = models.EmailField(verbose_name="email address", max_length=100, unique=True)
	
	#username = models.CharField(max_length=30, unique=True, blank=True, null=True)
	date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
	last_login = models.DateTimeField(verbose_name='last login', auto_now=True)
	is_admin = models.BooleanField(default=False)
	is_active = models.BooleanField(default=True)
	is_staff = models.BooleanField(default=False)
	is_superuser = models.BooleanField(default=False)

	date_of_birth = models.DateField(null=True, blank=True)
	avatar = models.CharField(max_length=255, null=True, blank=True)
	first_name = models.CharField(max_length=255, null=True, blank=True)
	last_name = models.CharField(max_length=255, null=True, blank=True)
	address = models.CharField(max_length=255, null=True, blank=True)
	contact_no = models.CharField(max_length=255, null=True, blank=True)

	objects = UserManager()

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['first_name', 'last_name']

	class Meta:
		verbose_name = ('user')
		verbose_name_plural = ('users')

	def get_full_name(self):
		return self.full_name

	def get_short_name(self):
		return self.first_name

	def __str__(self): 
		return self.full_name

	def has_perm(self, perm, obj=None):
		return self.is_admin

	def has_module_perms(self, app_label):
		return True

	@property
	def full_name(self):
		return '%s %s' % (self.first_name, self.last_name)
	

class UserAvatar(models.Model):
	user_id = models.ForeignKey(User,
		related_name='user_avatar',
		on_delete=models.CASCADE)
	avatar = models.FileField(upload_to='avatars/', null=True, blank=True)

	def __str__(self):
		return self.user_id.full_name