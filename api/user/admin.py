from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _
from user.forms import UserAdminCreationForm, UserAdminChangeForm
from user.models import *


# Register your models here.
@admin.register(User)
class UserAdmin(BaseUserAdmin):
	form = UserAdminChangeForm
	add_form = UserAdminCreationForm

	fieldsets = (
		(None,{'fields': ('email', 'password')}),
		(_('Personal info'), {'fields': ('first_name', 'last_name')}),
		(_('Permissions'), {'fields': ('is_admin','is_active',
			'is_staff')}),
		(_('Important dates'), {'fields': ('last_login','date_joined')}),
	)
	add_fieldsets = (
		(None, {
			'classes': ('wide',),
			'fields': ('email','first_name','last_name','password1','password2'),
		}),
	)
	list_display = ('id', 'email', 'last_name', 'first_name', 'date_joined', 'last_login', 'is_admin', 'is_staff')
	search_fields = ('email', 'last_name', 'first_name')
	readonly_fields = ('date_joined', 'last_login')
	ordering = ('email',)

	filter_horizontal = ()
	list_filter = ()

#admin.site.register(User, UserAdmin)
admin.site.register(UserAvatar)