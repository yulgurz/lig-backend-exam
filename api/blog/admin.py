from django.contrib import admin
from .models import *

@admin.register(Article)
class PostAdmin(admin.ModelAdmin):
	list_display = ('id', 'title', 'author', 'body', 'publish', 'created', 'updated', 'status')
	prepopulated_fields = {'slug': ('title',)}