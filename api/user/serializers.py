from rest_framework import serializers
from .models import *

class UserSerializer(serializers.ModelSerializer):
	full_name = serializers.SerializerMethodField()
	avatar = serializers.SerializerMethodField()
	class Meta:
		model = User
		fields = ['id', 'email', 'full_name', 'first_name', 'last_name', 'date_of_birth', 'contact_no', 'address', 'is_staff', 'avatar'];

	def get_full_name(self, instance):
		return instance.get_full_name()

	def get_avatar(self, instance):
		avatar = instance.user_avatar.all()
		return UserAvatarSerializer(avatar, many=True).data

class UserAvatarSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserAvatar 
		fields = ['id', 'avatar']