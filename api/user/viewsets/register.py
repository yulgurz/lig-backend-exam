from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from user.models import *
from user.serializers import *

class RegisterViewSet(viewsets.ModelViewSet):
	permission_classes=(AllowAny,)

	def create(self, request):
		if User.objects.filter(email=request.data['email']).exists():
			return Response({'message': 'User already exists.'}, status=status.HTTP_400_BAD_REQUEST) 

		customer = User.objects.create(
			email=request.data['email'],
			first_name=request.data['first_name'],
			last_name=request.data['last_name'],
		)
		customer.is_active=True
		customer.set_password(request.data['password'])
		customer.save()

		UserAvatar.objects.create(user_id=customer)
		return Response({'message': 'Signup successful.'}, status=status.HTTP_200_OK)