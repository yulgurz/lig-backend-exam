from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.parsers import FileUploadParser, MultiPartParser, FormParser, JSONParser
from django.utils.text import slugify
from .models import *
from .serializers import *

class BlogViewSet(viewsets.ModelViewSet):
	authentication_class = (TokenAuthentication, )
	permissions_class = (IsAuthenticated, )
	lookup_field = 'slug'

	def list(self, request):
		try:
			articles = Article.objects.filter(author=request.user)
			response = {
				'message': 'Success.',
				'articles': ArticleSerializer(articles, many=True).data
			}
		except:
			response = {
				'message': 'No articles.',
			}
		return Response(response, status=status.HTTP_200_OK)

	def retrieve(self, request, slug=None):
		try:
			article = Article.objects.get(slug=slug)
			response = {
				'message': 'Success.',
				'article': ArticleSerializer(article, many=False).data
			}
			return Response(response, status=status.HTTP_200_OK)
		except:
			response = {
				'message': 'Article does not exist.',
			}
			return Response(response, status=status.HTTP_400_BAD_REQUEST)


	def create(self, request):
		if(Article.objects.filter(title=request.data['title']).exists()):
			response = {
				'message': 'Article already exists.',
			}
			return Response(response, status=status.HTTP_400_BAD_REQUEST)

		article = Article.objects.create(
			author=request.user,
			title=request.data['title'],
			body=request.data['body'],
			status=request.data['status']
		)
		article.slug = slugify(request.data['title'])
		article.save()
		response = {
			'message': 'Article created.',
		}

		return Response(response, status=status.HTTP_200_OK)

	def update(self, request, slug=None):
		try:
			article = Article.objects.get(slug=slug)
			article.title=request.data['title'],
			article.body=request.data['body'],
			article.status=request.data['status']
			article.slug = slugify(request.data['title'])
			article.save()
			response = {
				'message': 'Article updated.',
			}
		except:
			response = {
				'message': 'Unable to update article.',
			}
		return Response(response, status=status.HTTP_200_OK)

	def destroy(self, request, slug=None):
		try:
			article = Article.objects.get(slug=slug)
			article.delete()
			response = {
				'message': 'Article deleted.',
			}
		except:
			response = {
				'message': 'Article does not exist.',
			}

		return Response(response, status=status.HTTP_200_OK)


