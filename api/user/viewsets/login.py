from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from user.models import *
from user.serializers import *
from .auth import *

class LoginViewSet(viewsets.ModelViewSet):
	queryset = User.objects.all()
	permission_classes = (AllowAny, )

	def create(self, request):		
		user = Users(request.data['username'],request.data['password'])
		user.authenticate_user()
		user.initialize_token()

		response = {
			'message': 'Login successful.',
			'token': user.get_token(),
			'user': user.get_user()
			}
		return Response(response, status=status.HTTP_200_OK)