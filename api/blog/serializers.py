from rest_framework import serializers
from .models import *
from user.models import *
from user.serializers import *

class ArticleSerializer(serializers.ModelSerializer):
	author = UserSerializer(many=False)
	class Meta:
		model = Article
		fields = ['id', 'title', 'slug', 'author', 'body', 'publish', 'created', 'updated', 'status'];