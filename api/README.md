# Django Article API

I did not create a virtual environment becase its casusing some issues on my vagrant file and in windows.
I just used the globally installed python libraries.
I also used the built-in sqlite for the database. I did not intergrate it with postgres so you can run it run easily.
I am also using python3.


## Installation

Install requirements

```bash
pip3 install -r requirements.txt
```

## Update models on django restframework auth token

For Ubuntu:

```bash
cd ~/.local/lib/python3.6/site-packages/rest_framework/authtoken
```

```bash
sudo nano models.py
```

Change key attriubute max_length=500 and save it.

Make migration and migrate changes.

Change directory to root api

## To run the django server:

```bash
python3 manage.py runserver
```

## API:

```bash
localhost:8000/api/login/
localhost:8000/api/register/
localhost:8000/api/blog/
```