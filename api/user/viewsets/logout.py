from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from user.models import *
from user.serializers import *

class LogoutViewSet(viewsets.ModelViewSet):
	permission_classes = (AllowAny, )

	def delete(self,request):
		try:
			request.user.auth_token.delete()
			return Response({'message': 'Successfully logged out.'}, status=status.HTTP_200_OK)
		except:
			return Response({'message': 'You are not logged in.'}, status=status.HTTP_400_BAD_REQUEST)

