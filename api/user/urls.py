from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from user.viewsets import *
from rest_framework_nested import routers

router = routers.DefaultRouter()
router.register('login', LoginViewSet, basename='login')
router.register('logout', LogoutViewSet, basename='logout')
router.register('register', RegisterViewSet, basename='register')

urlpatterns = [
	path('', include(router.urls)),
]
